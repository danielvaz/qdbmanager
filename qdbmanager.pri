QT       += sql
CONFIG   += c++11

INCLUDEPATH += $$PWD $$PWD/src

SOURCES += \
    $$PWD/src/sqlitedb.cpp \
    $$PWD/src/sqliteqttypematcher.cpp

HEADERS += \
    $$PWD/src/sqlitedb.h \
    $$PWD/src/sqliteqttypematcher.h \
    $$PWD/src/abstracttable.h
