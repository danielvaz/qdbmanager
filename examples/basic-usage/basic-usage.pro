QT += core
QT -= gui

CONFIG += c++11

TARGET = basic-usage
CONFIG += console
CONFIG -= app_bundle

include(../../qdbmanager.pri)

TEMPLATE = app

SOURCES += main.cpp \
    person.cpp

HEADERS += \
    person.h
