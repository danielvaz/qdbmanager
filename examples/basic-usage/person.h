#ifndef PERSON_H
#define PERSON_H

#include <QObject>

class Person : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(qint32 age READ age WRITE setAge NOTIFY ageChanged)
    Q_PROPERTY(Gender gender READ gender WRITE setGender NOTIFY genderChanged)

public:
    enum Gender { NotDeclared, Female, Male };
    Q_ENUM(Gender)

    explicit Person(QObject *parent = 0);

    QString name() const
    {
        return m_name;
    }

    qint32 age() const
    {
        return m_age;
    }

    Gender gender() const
    {
        return m_gender;
    }

    void setName(QString name)
    {
        if (m_name == name)
            return;

        m_name = name;
        emit nameChanged(name);
    }

    void setAge(qint32 age)
    {
        if (m_age == age)
            return;

        m_age = age;
        emit ageChanged(age);
    }

    void setGender(Gender gender)
    {
        if (m_gender == gender)
            return;

        m_gender = gender;
        emit genderChanged(gender);
    }

signals:
    void nameChanged(QString name);
    void ageChanged(qint32 age);
    void genderChanged(Gender gender);

private:
    QString m_name;
    qint32 m_age;
    Gender m_gender;
};

#endif // PERSON_H
