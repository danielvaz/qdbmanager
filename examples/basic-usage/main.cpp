#include <QCoreApplication>

#include "abstracttable.h"
#include "sqlitedb.h"
#include "person.h"

int main(int, char *[])
{
    SqliteDB db(QSqlDatabase::addDatabase("QSQLITE"));
    db.setDatabaseName("data.sqlite");
    db.openDB();
    AbstractTable<Person> personTable("persons", db);
    Person p;
    p.setName("David");
    p.setAge(3);
    p.setGender(Person::Male);
    //
    personTable.persist(&p);
    return 0;
}
